from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

from robot.api.deco import keyword

chrome_options = Options()

class TestsCodelab:

    @keyword("Accept cookies")
    def accept_cookies(self):
        driver = webdriver.Chrome(options=chrome_options)
        driver.get('https://codelab.eu/en/home-page')
        logo_element = driver.find_element(By.XPATH, '//*[@id="wt-cli-accept-all-btn"]')
        logo_element.click()
        return driver

    @keyword("Check logo")
    def check_logo(self, driver):
        logo_element = driver.find_element(By.XPATH, '//*[@id="view1"]/div/div[2]/div/div[1]/div/div/a')
        if logo_element.is_displayed():
            print("Codelab works!")
        else:
            print("Something went wrong :/")

    @keyword("Change language")
    def change_language(self, driver):
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-1334"]/a')
        logo_element.click()
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-1334"]/ul')
        logo_element.click()
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-1334-pl"]')
        logo_element.click()
        time.sleep(3)

    @keyword("Click services")
    def click_services(self, driver):
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-505"]/a')
        logo_element.click()

    @keyword("Check services")
    def check_services(self, driver):
        logo_element = driver.find_element(By.XPATH, '//*[@id="doorWay"]/div[1]/div[2]/div[1]/h1')
        if logo_element.is_displayed():
            print("Services work :)")
        else:
            print("Something went wrong :/")

if __name__ == '__main__':
    tests_codelab = TestsCodelab()
    driver = tests_codelab.accept_cookies()
    tests_codelab.click_services(driver)
    tests_codelab.check_services(driver)
