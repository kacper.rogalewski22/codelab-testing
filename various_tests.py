from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

chrome_options = Options()
driver = webdriver.Chrome(options=chrome_options)

class Tests_google:

    def accept_cookies():
        driver.get('https://www.google.com')
        logo_element = driver.find_element(By.XPATH, '//*[@id="L2AGLb"]/div')
        logo_element.click()

    def find_logo():
        logo_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/img')
        if logo_element.is_displayed():
            print("Google logo appeared!")
        else:
            print("Google logo did not appear.")

    def search_google(text):
        logo_element = driver.find_element(By.XPATH, '//*[@id="APjFqb"]')
        logo_element.send_keys(text)
        logo_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/form/div[1]/div[1]/div[4]/center/input[1]')
        logo_element.click()
        time.sleep(3)

    def go_to_codelab():
        logo_element = driver.find_element(By.XPATH, '//*[@id="rso"]/div[1]/div/div/div/div/div/div/div[1]/a/h3')
        logo_element.click()

class Tests_codelab:

    def check_logo():
        logo_element = driver.find_element(By.XPATH, '//*[@id="view1"]/div/div[2]/div/div[1]/div/div/a')
        if logo_element.is_displayed():
            print("Codelab działa!")
        else:
            print("Coś poszło nie tak :/")

    def accept_cookies():
        logo_element = driver.find_element(By.XPATH, '//*[@id="wt-cli-accept-all-btn"]')
        logo_element.click()

    def change_language():
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-1334"]/a')
        logo_element.click()
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-1334"]/ul')
        logo_element.click()
        logo_element = driver.find_element(By.XPATH, '//*[@id="menu-item-1334-pl"]')
        logo_element.click()
        time.sleep(3)

#main

text_1 = 'codelab.eu'
text_2 = 'zoo wrocław'

Tests_google.accept_cookies()
Tests_google.search_google(text_1)
Tests_google.go_to_codelab()

Tests_codelab.check_logo()
Tests_codelab.accept_cookies()
Tests_codelab.change_language()

Tests_google.accept_cookies()
Tests_google.search_google(text_2)

driver.quit()





